const express = require('express');
const cors = require('cors')
const app = express();
const http = require('http')
const server = http.createServer(app)
const bodyParser = require('body-parser')
const mortindoRoutes = require('./routes/mortindoRoutes')


const PORT = 81
app.use(express.json())
app.use(cors())
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use('/',mortindoRoutes)


// app.get('/', function(req, res) {
//     res.send('Hello World!');
// });


server.listen(PORT, () => {
    console.log(`The Service running well...in http://localhost:${PORT}`)
})