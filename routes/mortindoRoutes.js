const express = require('express')
const respone = require('../utils/respone.js')
const route = express.Router();
route.use(express.urlencoded({ extended: false }));
const mortindoController = require('../Controller/mortindoController')
const path = require('path')

route.get('/proyek',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        await mortindoController.getProyek(req,res)
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/proyekOne',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        await mortindoController.getOneProyek(req,res)
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/getSolusi',async(req,res)=>{
    console.log('as');
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        await mortindoController.getSolusi(req,res)
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/getProduk',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        await mortindoController.getProduk(req,res)
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/getBerita',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        await mortindoController.getBerita(req,res)
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/getTKDN',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        await mortindoController.getTKDN(req,res)
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/getProdukOne',async(req,res)=>{
    try {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        await mortindoController.getProdukOne(req,res)
    } catch (error) {
        console.log(error,`eror`);
        return res.send(respone("500",error))
    }
})
route.get('/imgproduct/:folder/:name', function(req, res) {
    const name = req.params.name;
    const folder = req.params.folder;
    const filename = name;
    const imagePath = path.join(__dirname, `../asset/${folder}`, filename);
    res.sendFile(imagePath);
});
route.get('/tds/:name', function(req, res) {
    const name = req.params.name;
    const filename = name;
    const imagePath = path.join(__dirname, `../asset/tds`, filename);
    res.sendFile(imagePath);
});
route.get('/berita/:name', function(req, res) {
    const name = req.params.name;
    const filename = name;
    const imagePath = path.join(__dirname, `../asset/berita`, filename);
    res.sendFile(imagePath);
});
module.exports = route