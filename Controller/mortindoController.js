// const respone = require('../../utils/respone')
const express = require('express')
const { DBCrm } = require('../config.js');

module.exports = {
    getProyek: async function (req, res) {
        try {
            let kategori  = req.query.kategori;

            const request = await DBCrm.promise();
            let filter = ``
            if(kategori){
                filter = filter+` and kategory_proyek = '${kategori}' `
            }
            let sel = `select * from m_proyek mp where isactive = 1 ${filter}`
            // console.log(sel);
            let ps = await request.query(sel);
            let data = ps[0]
            let result = []
            for(let i = 0; i<data.length; i++){
                let prod = data[i].product_use
                prod  = prod.replace(/\,/g,"','")
                prod = `'${prod}'`
                console.log(prod);

                let selProd = `select * from m_produk where nama_produk in (${prod})`
                let hsl = await request.query(selProd);
                hsl = hsl[0]
                let obj = {... data[i],products:hsl}
                result.push(obj)
            }

            res.send(result)
        } catch (error) {
            res.send(error)
        }
    },
    getOneProyek: async function (req, res) {
        try {
            let id  = req.query.id;

            const request = await DBCrm.promise();
            let sel = `select * from m_proyek mp where m_proyek_id = '${id}'`
            let ps = await request.query(sel);
            let prod = ps[0][0].product_use
            prod  = prod.replace(",","','")
            prod = `'${prod}'`

            let selProd = `select * from m_produk where nama_produk in (${prod})`
            let hsl = await request.query(selProd);
            const result = {
                proyek : ps[0][0],
                products : hsl[0]
            }
    
            res.send(result)
        } catch (error) {
            res.send(error)
        }
    },
    getProduk: async function(req,res){
        try {
            let kategori  = req.query.kategori;
            let key  = req.query.key;

            let filter = ``
            if(kategori){
                filter = filter+` and kategory = '${kategori}'`
            }
            if(key){
                filter = filter+` and (nama_produk like '%${key}%' or short_desc like '%${key}%' or description_1 like '%${key}%')`
            }
            const request = await DBCrm.promise();
            let sel = `select * from m_produk mp where isactive = 1 ${filter}`
            console.log(sel);
            let hsl = await request.query(sel);
            res.send(hsl[0])

        } catch (error) {
            res.send(error)
        }
    },
    getProdukOne : async function(req,res){
        try {
            let nama  = req.query.nama;
            let filter = ``
            if(nama){
                filter = filter+` and nama_produk = '${nama}' `
            }
            const request = await DBCrm.promise();
            let sel = `select * from m_produk mp where isactive = 1 ${filter}`
            let hsl = await request.query(sel);
            let kategory = hsl[0][0].kategory
            let selp = `select * from m_produk where isactive = 1 and kategory = '${kategory}'`;
            let dts = await request.query(selp);

            let objek = {
                data : hsl[0],
                rekomendasi : dts[0]
            }
            res.send(objek)

        } catch (error) {
            res.send(error)
        }
    },
    getSolusi: async function(req,res){
        try {
            const request = await DBCrm.promise();
            let sel = `select b.*,a.solusi,a.seq from m_produk_solusi a 
            inner join m_produk b on b.nama_produk  = a.nama_produk`;
            let hsl = await request.query(sel);
            res.send(hsl[0])
        } catch (error) {
            res.send(error)
        }
    },
    getBerita: async function(req,res){
        try {
            const request = await DBCrm.promise();
            let sel = `select * from m_berita where isactive = 1 order by createdate desc`;
            let hsl = await request.query(sel);
            res.send(hsl[0])
        } catch (error) {
            res.send(error)
        }
    },
    getTKDN: async function(req,res){
        try {
            const request = await DBCrm.promise();
            let sel = `select * from m_tkdn where isactive = 1`;
            let hsl = await request.query(sel);
            res.send(hsl[0])
        } catch (error) {
            res.send(error)
        }
    }

}
